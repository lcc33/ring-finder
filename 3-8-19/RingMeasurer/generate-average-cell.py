import numpy as np 
import matplotlib.pyplot as plt
from scipy import ndimage
from scipy import signal
from skimage import feature
import csv

def extract_cells(cell_file, angle, center_y, center_x):
  cell = []
  height = 40
  width = 10
  for ii in range(len(angle)):
    im = plt.imread(cell_file[ii])
    im_rot = ndimage.rotate(im, angle[ii])
    cell.append(im_rot[center_y[ii]-height:center_y[ii]+height, center_x[ii]-width:center_x[ii]+width])
  return cell

def import_data(file_name, column, convert_to_int):
  data = []
  with open(file_name, 'r') as data_file:
    data_import = csv.reader(data_file, delimiter=',')
    for row in data_import:
      data.append(row[column])
  data_file.close()
  if (convert_to_int):
    data = [int(ii) for ii in data]
  return data

def create_cell_average():
  cell_file_name = 'cell_centers.csv'
  cell_file = import_data(cell_file_name, 0, False)
  angle = import_data(cell_file_name, 1, True)
  center_y = import_data(cell_file_name, 2, True)
  center_x = import_data(cell_file_name, 3, True)
  cell = extract_cells(cell_file, angle, center_y, center_x)
  cell_average = np.add(cell[0], cell[1])
  return cell_average

def find_cells(cell_average):
  cell_average_rot = ndimage.rotate(cell_average, 40)
  cell_corr = signal.correlate2d(im, cell_average_rot,  boundary='symm', mode='same')
  ax = plt.subplot(rows, cols, 6)
  ax.imshow(cell_corr)
  radius = 50
  cell_corr_mean = np.sum(cell_corr)/(cell_corr.shape[0]*cell_corr.shape[1])
  for ii in range(20):
    ind = np.unravel_index(np.argmax(cell_corr), cell_corr.shape)
    ax = plt.subplot(rows, cols, 1)
    if (cell_corr[ind]>1.05*cell_corr_mean):
      ax.scatter(ind[1], ind[0], color='red', s=1)
      cell_corr[ind[0]-radius:ind[0]+radius, ind[1]-radius:ind[1]+radius] = 0

rows = 2
cols = 2

im = plt.imread('../0min/SNAP-165340-0003.tif')
ax = plt.subplot(rows, cols, 1)
ax.imshow(im)
kernel = np.ones((3, 3))
im = signal.convolve2d(im, kernel, boundary='symm', mode='same')

im_rot = ndimage.rotate(im, 55)
center = [1055, 928]
height = 40
width = 10
cell = im_rot[center[0]-height:center[0]+height, center[1]-width:center[1]+width]
ax = plt.subplot(rows, cols, 2)
ax.imshow(im_rot)
ax = plt.subplot(rows, cols, 3)
ax.imshow(cell)

cell_average = create_cell_average()
ax = plt.subplot(rows, cols, 4)
ax.imshow(cell_average)

#find_cells(cell_average)
plt.show()

with open("cell_average.csv","w+") as my_csv:
  csv_writer = csv.writer(my_csv, delimiter=',')
  csv_writer.writerows(cell_average)