import numpy as np 
import matplotlib.pyplot as plt
from scipy import ndimage
from scipy import signal
from skimage import feature
import csv
import math
import os
from os import listdir
from os.path import isfile, join
from tkinter import filedialog
from tkinter import *
from matplotlib.widgets import Button

class cell_finder():
  def __init__(self):
    self.edit_mode = False
    self.add_mode = True
    #self.file_name = '../0min/SNAP-165314-0002.tif'
    self.open_file()
    self.import_image(self.file_name)
    cell_average = self.import_data('cell_average.csv', float)
    #self.find_cells(cell_average)
    #self.cell_centers = [[631, 1150], [805, 162], [1096, 596], [915, 1178]]
    self.cell_centers = []
    self.plot_full_image()

  def open_file(self):
    path = os.path.dirname(os.path.realpath(__file__))
    root = Tk()
    root.withdraw()
    fileName =  filedialog.askopenfilenames(initialdir = path,title = "Select file")
    print(fileName[0])
    self.file_name = fileName[0]
    root.destroy()

  def import_image(self, file_name):
    im = plt.imread(file_name)
    im = im[10:,]
    self.im = np.pad(im, 100, 'mean')

  def plot_full_image(self):
    fig = plt.figure(0)
    plt.cla()
    ax = plt.axes()
    ax.imshow(self.im)
    for ii in range(len(self.cell_centers)):
      ax.scatter(self.cell_centers[ii][1], self.cell_centers[ii][0], color='red', s=1)
    cid = fig.canvas.mpl_connect('button_press_event', self.edit_cell_centers)
    cid = fig.canvas.mpl_connect('key_press_event', self.on_key_full)
    plt.show()
  
  def plot_single_cell(self):
    rows = 4
    cols = 2
    fig = plt.figure(self.active_cell+1)
    ax = plt.axes((0.05, 0.2, 0.4, 0.75))
    plt.cla()
    ax.imshow(self.single_cell)
    for jj in range(len(self.coordinates)):
      ax.scatter(self.coordinates[jj][1], self.coordinates[jj][0], color='red', s=10)
    ax = plt.axes((0.55, 0.2, 0.4, 0.75))      
    plt.cla()
    for jj in range(self.cross_sections.shape[0]):
      ax.plot(self.cross_sections[jj,])
    ax = plt.axes((0.05, 0.1, 0.2, 0.05))   
    button_adjust = Button(ax, "Adjust positioning")
    button_adjust.on_clicked(self.edit_cell_alignment)
    ax = plt.axes((0.3, 0.1, 0.2, 0.05))   
    button_go = Button(ax, "Use positioning")
    button_go.on_clicked(self.reanalyze_cell)
    ax = plt.axes((0.05, 0.05, 0.2, 0.05))   
    button_export = Button(ax, "Export")
    button_export.on_clicked(self.export)
    ax = plt.axes((0.3, 0.05, 0.2, 0.05))   
    button_skip = Button(ax, "Skip")
    button_skip.on_clicked(self.skip)
    cid = fig.canvas.mpl_connect('button_press_event', self.on_click)
    plt.show()  
      
  def on_key_full(self, event):
    if (event.key=='enter'):
      plt.close()
      self.begin_cell_analysis()
  
  def export(self, event):
    plt.close()
    file_name = 'images/' + self.file_name[-15:-4] +'_cell_'+ str(self.active_cell+1) + '.csv'
    with open(file_name, 'w', newline='') as csvfile:
      writer = csv.writer(csvfile, delimiter=',')
      for ii in range(self.single_cell.shape[0]):
        writer.writerow(self.single_cell[ii,:])
  
  def skip(self, event):
    plt.close()
      
  def on_click(self, event):
    if (self.edit_mode):
      self.coordinates.append([int(event.ydata), int(event.xdata)])
      self.plot_single_cell()

  def edit_cell_alignment(self, event):
    self.cell_zoom_out()
    self.edit_mode = True
    self.coordinates = []
    self.plot_single_cell()
      
  def edit_cell_centers(self, event):
    location = [event.ydata, event.xdata]
    close_points = 0
    remove_points = []
    for ii in range(len(self.cell_centers)):
      center = self.cell_centers[ii]
      distance_between = math.sqrt((center[1]-location[1])**2 + (center[0]-location[0])**2)
      if (distance_between < 20):
        close_points += 1
        remove_points.append(ii)
    if (close_points<1):
      ycoor = int(location[0])
      xcoor = int(location[1])
      self.cell_centers.append([ycoor, xcoor])
    for ii in range(len(remove_points)):
      self.cell_centers.pop(remove_points[ii])
    self.plot_full_image()

  def begin_cell_analysis(self):
    for self.active_cell in range(len(self.cell_centers)):
      self.coordinates = []
      self.straighten_cell()
      self.analyze_single_cell()
      self.plot_single_cell()
      
  def reanalyze_cell(self, event):
    if (len(self.coordinates)>1):
      self.edit_mode = False
      self.recenter_cell()
      self.analyze_single_cell()
      self.coordinates = []
      self.plot_single_cell()
    else: 
      self.analyze_single_cell()
      self.plot_single_cell()
    plt.close('all')

  def import_data(self, file_name, data_type):
    data = np.loadtxt(open(file_name, "rb"), delimiter=",", skiprows=1).astype(data_type)
    return data
  
  def find_cells(self, cell_average):
    self.cell_centers = []
    radius = 20
    angle = 0
    while (angle<=180):
      cell_corr_total = np.zeros(self.im.shape)
      cell_average_rot = ndimage.rotate(cell_average, angle)
      cell_corr = signal.correlate2d(self.im, cell_average_rot,  boundary='symm', mode='same')
      cell_corr_total = np.add(cell_corr, cell_corr_total)
      angle += 45
    cell_corr_mean = np.sum(cell_corr_total)/(cell_corr.shape[0]*cell_corr.shape[1])
    for ii in range(100):
      ind = np.unravel_index(np.argmax(cell_corr_total), cell_corr.shape)
      if (cell_corr_total[ind]>1.05*cell_corr_mean):
        self.cell_centers.append(ind)
        cell_corr_total[ind[0]-radius:ind[0]+radius, ind[1]-radius:ind[1]+radius] = 0

  def straighten_cell(self):
    center = self.cell_centers[self.active_cell]
    cell_ideal = np.zeros((100, 30))
    cell_ideal[25:75, 10:20] = 1
    angle = np.linspace(0, 180, num=73)
    angle_fit = np.zeros(len(angle))
    for jj in range(len(angle)):
      cell_rot = ndimage.rotate(cell_ideal, angle[jj])
      height = int(cell_rot.shape[0]/2)
      width = int(cell_rot.shape[1]/2)
      cell_rot = cell_rot[0:height*2,0:width*2]
      single_cell = self.im[center[0]-height:center[0]+height, center[1]-width:center[1]+width]
      angle_fit[jj] = np.sum(np.multiply(single_cell, cell_rot))
    self.crop_rotate_im(center, 360-angle[np.argmax(angle_fit)])

  def crop_rotate_im(self, center, angle):
    height = 100
    width = 100
    single_cell = self.im[center[0]-height:center[0]+height, center[1]-width:center[1]+width]
    single_cell = ndimage.rotate(single_cell, angle)
    height = 50
    width = 30
    center_y = int(single_cell.shape[0]/2)
    center_x = int(single_cell.shape[1]/2)
    self.single_cell = single_cell[center_y-height:center_y+height, center_x-width:center_x+width] 
    
  def analyze_single_cell(self):
    cross_section = np.sum(self.single_cell[:, 28:33], axis=1)
    bottom_ind, top_ind = self.fwhm_boundaries(cross_section)
    center_y = int(np.mean([bottom_ind, top_ind]))
    quarter_y = int(np.mean([bottom_ind, center_y]))
    print(quarter_y, center_y)
    offset = [-quarter_y, 0, quarter_y]
    self.cross_sections = np.zeros((len(offset), self.single_cell.shape[1]))
    for ii in range(len(offset)):
      cell_slice = self.single_cell[center_y+offset[ii]-2:center_y+offset[ii]+3,]
      self.cross_sections[ii,] = np.mean(cell_slice, axis=0)
    quarter_mean = np.add(self.cross_sections[0,], self.cross_sections[2,])
    quarter_mean = np.divide(quarter_mean, 2)
    quarter_mean_sort = np.sort(quarter_mean)    
    quarter_max = np.mean(quarter_mean_sort[-6:-1])
    quarter_min = np.mean(quarter_mean_sort[0:5])
    center_mean_sort = np.sort(self.cross_sections[1,])    
    center_max = np.mean(center_mean_sort[-6:-1])
    center_intensity_difference = (center_max-quarter_min)/(quarter_max-quarter_min)

  def fwhm_boundaries(self, cross_section):
    center_x = int(len(cross_section)/2)
    cross_section = cross_section - np.min(cross_section)
    min_mean = np.mean([cross_section[0:5], cross_section[-6:-1]])
    max_mean = np.mean(np.sort(cross_section)[-4:-1])
    mid = (max_mean-min_mean)/2
    distance_from_mid = np.abs(np.subtract(cross_section, mid))
    left_ind = np.argmin(distance_from_mid[0:center_x])
    right_ind = np.argmin(distance_from_mid[center_x:-1])+center_x
    return left_ind, right_ind

  def cell_zoom_out(self):
    height = 100
    width = 100
    center = self.cell_centers[self.active_cell]
    self.single_cell = self.im[center[0]-height:center[0]+height, center[1]-width:center[1]+width]

  def recenter_cell(self):
    center = [0, 0]
    top_point = np.argmin([self.coordinates[-1][0], self.coordinates[-2][0]])
    center[0] = int((self.coordinates[-3][0] + self.coordinates[-2][0])/2)
    center[1] = int((self.coordinates[-3][1] + self.coordinates[-2][1])/2)
    center = np.subtract(center, 100)
    if (top_point==0):
      dy = self.coordinates[-2][0] - self.coordinates[-3][0]
      dx = self.coordinates[-2][1] - self.coordinates[-3][1]
    else:
      dy = self.coordinates[-3][0] - self.coordinates[-2][0]
      dx = self.coordinates[-3][1] - self.coordinates[-2][1]
    angle = 360 - int(np.arctan2(dx, dy)*180/3.14)
    center = np.add(self.cell_centers[self.active_cell], center)
    self.cell_centers[self.active_cell] = center
    self.crop_rotate_im(center, angle) 

  
plot = cell_finder()
